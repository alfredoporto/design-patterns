public class CoffeeTouchscreenAdapter implements ICoffeMaker {
    private OldCoffeeMachine oldCoffeeMachine ;

    public CoffeeTouchscreenAdapter(OldCoffeeMachine oldCoffeeMachine){
        this.oldCoffeeMachine = oldCoffeeMachine;
    }

    @Override
    public void chooseFirstSelection() {
        this.oldCoffeeMachine.selectA();
    }

    @Override
    public void chooseSecondSelection() {
        this.oldCoffeeMachine.selectB();
    }
}
