package assessment;

public class Song implements IComponent {
    private String songName;
    private String artist;
    private float speed;

    @Override
    public void play() { }
    @Override
    public void setPlaybackSpeed(float speed) { }
    @Override
    public String getName() {
        return this.songName;
    }
    public String getArtist() {
        return this.getArtist();
    }
}
