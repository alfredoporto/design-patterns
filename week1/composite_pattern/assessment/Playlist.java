package assessment;

import java.util.ArrayList;

public class Playlist implements IComponent {
    private String playlistName;
    private ArrayList<IComponent> playlist;

    public Playlist(String name){
        this.playlist = new ArrayList<IComponent>();
        this.playlistName = name;
    }
    @Override
    public void play() {
        for(IComponent song: this.playlist)
            song.play();}
    @Override
    public void setPlaybackSpeed(float speed) { }
    @Override
    public String getName() {
        return this.playlistName;
    }

    public void add(IComponent component){
        this.playlist.add(component);
    }
    public void remove(IComponent component){
        this.playlist.remove(component);
    }
}
