package example;

import java.util.ArrayList;

public class Housing implements IStructure {
    private ArrayList<IStructure> structures;
    private String address;

    public Housing (String address){
        this.structures = new ArrayList<IStructure>();
        this.address = address;
    }

    public int addStructure(IStructure component){
        this.structures.add(component);
        return this.structures.size() - 1;
    }
    public IStructure getStructure(int componentIndex){
        return this.structures.get(componentIndex);
    }
    @Override
    public void location(){
        System.out.println("You are currently in " + this.getName() +
                ". It has ");
        for(IStructure structure: this.structures)
            System.out.println(structure.getName());
    }
    @Override
    public void enter() {}
    @Override
    public void exit() {}
    @Override
    public String getName(){return "name";}
}
